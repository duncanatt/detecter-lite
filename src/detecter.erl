%%%-------------------------------------------------------------------
%%% @author duncan
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 11. May 2016 13:54
%%%-------------------------------------------------------------------
-module(detecter).
-author("duncan").

-include_lib("stdlib/include/assert.hrl").

-ifndef(BASE_DIR).
-define(BASE_DIR, ".").
-endif.
-define(TEMP_DIR, "temp").
-define(INCLUDE_DIR, "include").
-define(LAUNCHER_NAME, "launcher.erl").

%% API
-export([instrument/1, instrument/3]).

instrument([ScriptFile, AppBinDir, MFA]) ->

  io:format("Starting instrumentation with the following parameters:~n
  \tScript File: ~s~n\tApp Base Dir: ~s~n\tMFA: ~s~n~n",
    [ScriptFile, AppBinDir, MFA]),

  % Validate parameters.
  require(ScriptFile, "Script File"),
  require(AppBinDir, "App Bin Directory"),
  require(MFA, "MFA Tuple"),

  % Parse MFA tuple from string; this is used to start the target system.
  ParsedMFA = util:parse_erl_expr(MFA),

  % Instrument the target system by first generating the monitor from the
  % HML script file, copying all the necessary dependencies and finally
  % compiling everything into the target system binary directory. A launcher
  % class is generated to permit the user to execute a monitored system.
  instrument(ScriptFile, AppBinDir, ParsedMFA).




instrument(ScriptFile, AppBinDir, MFA) ->

  % Check if app binary directory exists.
  case filelib:is_dir(AppBinDir) of
    false -> error(util:format("App Binary Directory '~s' does not exist",
      [AppBinDir]));
    _ -> ok
  end,

  % Get the base file name of the script file. This will be used as the monitor
  % module file name.
  ModName = to_mod_name(ScriptFile),

  % Set up the necessary directories and files.
  TempDir = abs_dir(?TEMP_DIR),
  MonDir = abs_dir(filename:join("src", "mon")),
  MonFile = filename:join(TempDir, ModName ++ ".erl"),
  LauncherFile = filename:join(TempDir, ?LAUNCHER_NAME),

  io:format("Creating module '~s' from script file '~s'.~n",
    [ModName, ScriptFile]),

  % Compile script file into monitor.
  {ok, Code, Subjects} = compiler:build(util:file_to_str(ScriptFile), ModName),

  % Write the monitor code to file.
  util:code_to_file(MonFile, Code),

  % Create launcher module.
  util:code_to_file(LauncherFile, create_launcher(ModName, MFA, Subjects)),

  % Copy supporting module files.
  copy_files_to_dir(MonDir, TempDir, "*.erl"),

  % Compile resulting Erlang files into .beam files.
  compile_files_in_dir(TempDir),

  % Copy compiler monitor file and supporting files.
  copy_files_to_dir(TempDir, AppBinDir, "*.beam"),

  % Remove files from temporary directory, and finally also the temporary
  % directory.
  del_files(TempDir, "*.{beam,erl}"),
  file:del_dir(TempDir).

copy_file(SrcDir, DestDir, File) ->
  io:format("Copying file ~s from ~s to ~s.~n", [File, SrcDir, DestDir]),
  file:copy(filename:join(SrcDir, File), filename:join(DestDir, File)).

copy_files_to_dir(SrcDir, DestDir, Ext) ->
  lists:foreach(fun(F) -> copy_file(SrcDir, DestDir, filename:basename(F)) end,
    filelib:wildcard(filename:join(SrcDir, Ext))).

del_file(SrcDir, File) ->
  io:format("Deleting file ~s from ~s.~n", [File, SrcDir]),
  file:delete(filename:join(SrcDir, File)).

del_files(SrcDir, Ext) ->
  lists:foreach(fun(F) -> del_file(SrcDir, filename:basename(F)) end,
    filelib:wildcard(filename:join(SrcDir, Ext))).

%% Extracts a suitable simple module name from the specified filename.
to_mod_name(Filename) ->
  util:keep_chars(filename:basename(Filename, ".hml"),
    lists:seq(97, 122) ++ "_" ++ "@").


%% Generates the Erlang code from the specified monitor structure and subjects
%% into a module file with the name specified by module.
create_launcher(Module, MFA, Subjects) ->
  Body = util:format("start() ->
          main_mon:start(~p, ~s:property(), ~p).",
    [MFA, Module, Subjects]),

  util:format("-module(launcher).~n-export([start/0]).~n~s", [Body]).

compile_files_in_dir(Dirname) ->
  lists:foreach(fun compile_file/1, filelib:wildcard(filename:join(Dirname, "*.erl"))).

compile_file(Filename) ->
  io:format("Compiling source file: ~s...~n", [Filename]),
%%  Status = compile:file(Filename, [{outdir, ?TEMP_DIR}, {d, debug}, {i, ?INCLUDE_DIR}, report, {warn_format, 2}]),
  compile:file(Filename, [{outdir, ?TEMP_DIR}, {i, ?INCLUDE_DIR}, report, {warn_format, 2}])
%%  compile:format_error(Status),
%%  io:format("~p.~n", [Status]),

%%  {"temp/main_mon.erl",[{11,epp,{redefine,debug}}]}
%%  Messages = case Status of
%%    {ok, _File, WarnList} -> ok;
%%    {error, [{_, ErrorDescList}], [{_, WarningDescList}]} ->
%%      util:format("failed.~n~s~s", [to_msg_str(ErrorDescList, "Error(s)"), to_msg_str(WarningDescList, "Warning(s)")])
%%  end,
%%
%%  io:format("~p~n", [Status])
.


%%to_msg_str(DescriptorList, Msg) ->
%%  case to_msg_list(DescriptorList, []) of
%%    Msgs when length(Msgs) > 0 -> util:format("~s:~n~s~n", [Msg, Msgs]);
%%    _ -> ""
%%  end.
%%to_msg_list([], Acc) -> lists:reverse(Acc);
%%to_msg_list([{Line, Module, Descriptor} | T], Acc) ->
%%  to_msg_list(T, [util:format("at line ~p: ~s~n",
%%    [Line, apply(Module, format_error, [Descriptor])]) | Acc]).

require(Param, Name) ->
  case Param of
    P when length(P) =< 0 -> throw(util:format("~s cannot be empty", [Name]));
    _ -> ok
  end.

abs_dir(Dir) -> filename:join(?BASE_DIR, Dir).




