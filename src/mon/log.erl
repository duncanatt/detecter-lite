%%%-------------------------------------------------------------------
%%% @author duncan
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 25. Feb 2016 10:42 PM
%%%-------------------------------------------------------------------
-module(log).
-author("duncan").

%%-define(log, false).
-include("log.hrl").

-import(lists, [reverse/1]).

%% API
-export([write/4, write/5, debug_str/1, debug_str/2, trace_str/1, trace_str/2]).
%%-compile(export_all).



debug_str(Format) -> debug_str(Format, []).

debug_str(Format, Params) ->
  case ?debug of
    true -> log_str(?debug_str, Format, Params);
    _ -> ""
  end.

trace_str(Format) -> trace_str(Format, []).

trace_str(Format, Params) ->
  case ?debug of
    true -> log_str(?trace_str, Format, Params);
    _ -> ""
  end.

%%trace_string(Format) ->
%%  case ?trace of
%%    true -> log_string(?TRACE, Format);
%%    _ -> ""
%%  end.
%%
%%trace_string(Format, Params) ->
%%  case ?trace of
%%    true -> log_string(?DEBUG, Format, Params);
%%    _ -> ""
%%  end.

%%debug(Format) -> debug(Format, []).
%%
%%debug(Format, Params) ->
%%  case ?debug of
%%    true -> fun() -> io:format("LOGGING ENABLED ~p", [?MODULE]) end;
%%    _ -> fun() -> ok end
%%  end.


write(Level, Module, Line, Format) -> write(Level, Module, Line, Format, []).

write(Level, Module, Line, Format, Params) ->
  case can_log(?log_level, Level) of
    true ->
      io:fwrite(user, "[~s, ~s - ~p - ~p:~p] - ~s~n",
        [current_time(), Level, self(), Module, Line, io_lib:format(Format, Params)]);
    false -> ok
  end.

%% Determines whether log messages can be produced, given the specified trace
%% level and trace string.
can_log(?trace_level, ?trace_str) -> true;
can_log(?debug_level, ?trace_str) -> true;
can_log(?debug_level, ?debug_str) -> true;
can_log(?info_level, ?trace_str) -> true;
can_log(?info_level, ?debug_str) -> true;
can_log(?info_level, ?info_str) -> true;
can_log(_, _) -> false.


%%log_str(Level, Format) ->
%%  io_lib:format("?~s(\"~s\"),",
%%    [Level, Format]).

log_str(Level, Format) -> log_str(Level, Format, []).

log_str(Level, Format, Params) ->
  case length(Params) of
    0 -> io_lib:format("?~s(\"~s\"),", [Level, Format]);
    _ -> io_lib:format("?~s(\"~s\", [~s]),", [Level, Format, params_as_string(Params)])
  end.

%%log_string_old(Level, Format) ->
%%  io_lib:format("io:format(\"~s - ~~p:~~p - ~s~~n\", [?MODULE, ?LINE]),",
%%    [Level, Format]).
%%
%%log_string_old(Level, Format, Params) ->
%%  io_lib:format("io:format(\"~s - ~~p:~~p - ~s~~n\", [?MODULE, ?LINE, ~s]),",
%%    [Level, Format, params_as_string(Params)]).




params_as_string(Params) ->
  params_as_string(Params, []).

params_as_string([Hd | Tl], Acc) ->
  Pattern =
    case io_lib:printable_unicode_list(Hd) of
      true -> io_lib:format("~s", [Hd]);
      _ -> io_lib:format("~p", [Hd])
    end,
  params_as_string(Tl, [Pattern | Acc]);

params_as_string([], Acc) -> string:join(reverse(Acc), ",").

-spec current_time() -> string().
%% @doc Returns a pretty-printed, log-friendly  timestamp of the current time.
current_time() ->
  {{Year,Month,Day}, {Hr, Min, Sec}} = erlang:localtime(),
  FmtTimeDate = io_lib:format("~B/~B/~4..0B ~2B:~2.10.0B:~2.10.0B", [Day,Month,Year, Hr, Min, Sec]),
  lists:flatten(FmtTimeDate).