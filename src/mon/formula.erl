%%%-------------------------------------------------------------------
%%% @author duncan
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @reference See
%%% <a href="http://staff.um.edu.mt/afra1/papers/rv15rechml.pdf">On Verifying
%%% Hennessy-Milner Logic with Recursion at Runtime</a> for more information.
%%% @reference See
%%% <a href="http://staff.um.edu.mt/afra1/papers/rv13jour.pdf">Synthesising
%%% correct concurrent runtime monitors</a> for more information.
%%% @end
%%% Created : 22. Feb 2016 4:19 PM
%%%-------------------------------------------------------------------
-module(formula).
-author("duncan").

-include("macros.hrl").
-include("log.hrl").


%% API
-export([mon_id/0, mon_ff/0, mon_tt/0, mon_and/2, mon_or/2, mon_nec/1,
  mon_pos/1, mon_max/2, mon_min/2, mon_var/1, mon_guard/3]).

-type environment() :: [{var(), formula()}].
%% Describes the formula environment (map) mapping variables to formulae.

-type formula() :: fun((environment()) -> atom() | formula()).
%% Describes the recursive definition of a formula represented in terms of a
%% function which given an environment, returns either an atom or a sub-formula.
%% This compositional definition of formulae enables the formulae to ...

-type var() :: atom().
%%

-type action_matcher() :: fun((action()) -> formula()).
%%

-type action() :: {event(), subject(), msg()}.
%%

-type event() :: recv | send | call | rtrn.
%%

-type subject() :: pid().
%%

-type msg() :: term().
%%

-spec mon_id() -> Phi when
  Phi :: formula().
%% @doc Returns the identity formula when the monitor is unable to perform a
%% match, and denotes the monitor ending with an inconclusive
%% verdict.
mon_id() ->
  fun(_Env) ->
    ?DEBUG("mon_id no match.")
  end.

-spec mon_ff() -> Phi when
  Phi :: formula().
%% @doc Returns the falsity formula denoting a property violation.
mon_ff() ->
  fun(_Env) ->
    ?DEBUG("mon_ff matched 'ff' action."),
    sup ! ff
  end.

-spec mon_tt() -> Phi when
  Phi :: formula().
%% @doc Returns the truth formula denoting a property satisfaction.
mon_tt() ->
  fun(_Env) ->
    ?DEBUG("mon_tt matched 'tt' action."),
    sup ! tt
  end.

-spec mon_and(Phi, Psi) -> Chi when
  Phi :: formula(),
  Psi :: formula(),
  Chi :: formula().
%% @doc Returns the formula performing the conjunction of the two specified
%% formulae Phi and Psi.
mon_and(Phi, Psi) ->
  fun(Env) ->
    ProcCount = length(processes()),
    PhiPid = spawn_link(fun() -> Phi(Env) end),
    PsiPid = spawn_link(fun() -> Psi(Env) end),
    ?DEBUG("mon_and forked processes '~p' and '~p' (incremented processes from ~p to ~p).", [PhiPid, PsiPid, ProcCount, length(processes())]),
    fork(PhiPid, PsiPid)
  end.

-spec mon_or(Phi, Psi) -> Chi when
  Phi :: formula(),
  Psi :: formula(),
  Chi :: formula().
%% @doc Returns the formula performing the disjunction of the two specified
%% formulae Phi and Psi.
mon_or(Phi, Psi) ->
  fun(Env) ->
    ProcCount = length(processes()),
    PhiPid = spawn_link(fun() -> Phi(Env) end),
    PsiPid = spawn_link(fun() -> Psi(Env) end),
    ?DEBUG("mon_or forked processes '~p' and '~p' (incremented processes from ~p to ~p).", [PhiPid, PsiPid, ProcCount, length(processes())]),
    fork(PhiPid, PsiPid)
  end.

-spec mon_nec(action_matcher()) -> Phi when
  Phi :: formula().
%% @doc Returns the formula Phi which performs a trace event match against the
%% action pattern using the specified ActionMatcher function. If the match
%% succeeds, the <b>closed</b> continuation sub-formula is returned by Phi,
%% otherwise the identity formula is returned instead (see  {@link mon_id/0}).
mon_nec(ActionMatcher) ->
  fun(Env) ->
    receive
      {action, Action} -> % Action is {Receiver, Msg}
        ?TRACE("mon_nec evaluating action: ~p.", [Action]),
        ClosedPhi = ActionMatcher(Action),
        ClosedPhi(Env)
    end
  end.

-spec mon_pos(action_matcher()) -> Phi when
  Phi :: formula().
%% @doc Returns the formula Phi which performs a trace event match against the
%% action pattern using the specified ActionMatcher function. If the match
%% succeeds, the <b>closed</b> continuation sub-formula is returned by Phi,
%% otherwise the identity formula is returned instead (see  {@link mon_id/0}).
mon_pos(ActionMatcher) ->
  fun(Env) ->
    receive
      {action, Action} -> % Action is {Receiver, Msg}
        ?TRACE("mon_pos evaluating action: ~p.", [Action]),
        ClosedPhi = ActionMatcher(Action),
        ClosedPhi(Env)
    end
  end.

-spec mon_max(Var, Phi) -> Psi when
  Var :: var(),
  Phi :: formula(),
  Psi :: formula().
%% @doc Returns the formula Psi which stores the specified Phi-Var mapping into
%% the environment before applying it to Phi. The sub-formula resulting from
%% the application of Phi to the new updated environment is then returned by
%% Psi.
mon_max(Var, Phi) ->
  fun(Env) ->
    ?DEBUG("mon_max adding variable ~p to formula environment.", [Var]),
    Phi([{Var, Phi} | Env])
  end.

-spec mon_min(Var, Phi) -> Psi when
  Var :: var(),
  Phi :: formula(),
  Psi :: formula().
%% @doc Returns the formula Psi which stores the specified Phi-Var mapping into
%% the environment before applying it to Phi. The sub-formula resulting from
%% the application of Phi to the new updated environment is then returned by
%% Psi.
mon_min(Var, Phi) ->
  fun(Env) ->
    ?DEBUG("mon_min adding variable ~p to formula environment.", [Var]),
    Phi([{Var, Phi} | Env])
  end.

-spec mon_var(Var) -> Phi when
  Var :: var(),
  Phi :: formula().
%% @doc Returns the formula Phi which given the specified environment variable
%% Var, fetches the corresponding sub-formula and applies it to the formula
%% environment, returning the result. If no such formula for Var is found in the
%% environment, then execution halts (this should not happen since variables in
%% formulae are assumed to be guarded).
mon_var(Var) ->
  fun(Env) ->
    ?DEBUG("mon_var retrieving variable ~p from formula environment and recursing.", [Var]),
    Phi = look_up(Var, Env),
    Phi(Env)
  end.

-spec mon_guard(Expr, Phi, Psi) -> Chi when
  Expr :: term(),
  Phi :: formula(),
  Psi :: formula(),
  Chi :: formula().
%% @doc Currently not supported and is work in progress.
mon_guard(Expr, Phi, Psi) ->
  fun(Env) ->
    case Expr of
      true ->
        ?DEBUG("mon_guard guard evaluated to true."),
        Phi(Env);
      false ->
        ?DEBUG("mon_guard guard evaluated to false."),
        Psi(Env);
      _ -> invalid_condition
    end
  end.

-spec fork(PhiPid, PsiPid) -> ok when
  PhiPid :: pid(),
  PsiPid :: pid().
%% @doc Copies and forwards received trace event messages to the two specified
%% PIDs corresponding to the sub-monitor actors for formulae Phi and Psi.
fork(PhiPid, PsiPid) ->
  receive
    Msg ->
      PhiPid ! Msg,
      PsiPid ! Msg
  end,
  fork(PhiPid, PsiPid).

-spec look_up(Var, Env) -> Phi | undefined_fixpoint_variable when
  Var :: var(),
  Env :: environment(),
  Phi :: formula().
%% @doc Fetches and returns the formula corresponding to the specified variable
%% Var in the given formula environment. If no such formula is found, the atom
%% <b>undefined_fixpoint_variable</b> is returned instead. Note that since
%% formulae variables are assumed to be guarded, this should never occur as
%% each variable ought to have a corresponding formula associated with it in the
%% formula environment.
look_up(Var, Env) ->
  case Env of
    [{Var, Phi} | _T] -> Phi;
    [{_, _} | T] -> look_up(Var, T);
    [] -> undefined_fixpoint_variable % Should not happen since we assume guarded variables.
  end.