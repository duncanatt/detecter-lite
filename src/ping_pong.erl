%%%-------------------------------------------------------------------
%%% @author duncan
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 21. May 2016 17:08
%%%-------------------------------------------------------------------
-module(ping_pong).
-author("duncan").

-define(PING, ping_client).
-define(PONG, pong_server).

%% API
-export([pong_start/0, pong_stop/0, ping_start/1, ping_stop/0, tiny_tracer_start/1]).

%% Starts the pong server.
pong_start() ->
  register(?PONG, spawn_link(fun() ->
    io:format("Started PONG server...~n"),
    pong_loop()
  end)).

%% Stops the pong server.
pong_stop() -> ?PONG ! {self(), stop}.

%% Processes ping requests.
pong_loop() ->
  receive
    {Pid, ping} ->
      io:format("Received PING.~n"),
      Pid ! {self(), pong},
      pong_loop();
    {Pid, stop} ->
      io:format("Stopping PONG server.~n"),
      Pid ! {self(), ok},
      group_leader() ! {self(), stop},
      true = unregister(?PONG);
    _U ->
      io:format("Unknown msg: ~p.~n", [_U]),
      pong_loop()
  end.

ping_start(Interval) ->
  register(?PING, spawn_link(fun() ->
    io:format("Started PING client with interval of ~p...~n", [Interval]),
    ping_loop(Interval)
  end)).

ping_stop() -> ?PING ! {self(), stop}.

%% Sends ping requests.
ping_loop(Interval) ->
  timer:sleep(Interval),
  ?PONG ! {self(), ping},
  receive
    {Pid, pong} ->
      io:format("Received back PONG.~n"),
      ping_loop(Interval);
    {Pid, stop} ->
      io:format("Stopping PING server.~n"),
      Pid ! {self(), ok},
      group_leader() ! {self(), stop},
      true = unregister(?PING)
  end.

tiny_tracer_start(ProcName) ->

  % Get the PID of the process to be monitored.
  TargetPid = whereis(ProcName),
  io:format("TargetPid = ~p~n", [TargetPid]),

  % Get the PID of the tracer process.
  TracerPid = spawn(fun() ->
    io:format("Started tiny tracer for proc name ~p [PID: ~p].~n", [ProcName, TargetPid]),
    tiny_tracer_loop(ProcName)
  end),

  % Set up the tracing to trace to another tracer, not the caller.
  erlang:trace(TargetPid, true, [{tracer, TracerPid}, send, 'receive']),

  TracerPid.

tiny_tracer_loop(ProcName) ->
  receive
    stop ->
      io:format("Stopping tracer for proc name ~p [PID: ~p].~n", [ProcName, whereis(ProcName)]);
    Msg ->
      io:format("TR-~p: Received trace event: ~p.~n", [ProcName, Msg]),
      tiny_tracer_loop(ProcName)
  end.