%%%-------------------------------------------------------------------
%%% @author duncan
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 31. Mar 2016 22:06
%%%-------------------------------------------------------------------
-module(compiler).
-author("duncan").

-include("macros.hrl").
-import(lists, [reverse/1, mapfoldl/3, concat/1, map/2, filter/2, duplicate/2, flatten/1]).

-define(LEXER, lexer).
-define(PARSER, parser).
-define(MONITOR, formula).

%%
%% Exported Functions
%%
-compile(export_all).


%% Tokenizes HML formula string.
tokenize(Property) ->
  io:format("Performing lexical analysis...~n"),
  case ?LEXER:string(Property) of
    {ok, Tokens, _} -> {ok, Tokens};
    {error, {Line, lexer, {illegal, Token}}, _} ->
      {error, {lexer, Line, Token},
        util:format("Illegal token: ~s, line ~p", [Token, Line])}
  end.

%% Parses list of HML tokens to produce syntax tree.
parse(Tokens) ->
  io:format("Parsing...~n"),
  case ?PARSER:parse(Tokens) of
    {ok, SyntaxTree} -> {ok, SyntaxTree};
    {error, {Line, parser, [_, Token]}} ->
      {error, {parser, Line, Token},
        util:format("Syntax error before: '~s', line ~p.", [Token, Line])}
  end.

%% Builds the syntax tree and extracts the subjects from the specified HML
%% property.
analyze(Property) ->
  io:format("Performing analysis...~n"),
  case tokenize(Property) of
    {ok, Tokens} ->
      case parse(Tokens) of
        {ok, SyntaxTree} -> {ok, get_subjects(Tokens), SyntaxTree};
        {error, _, Message} -> {error, Message}
      end;
    {error, _, Message} -> {error, Message}
  end.


%% Synthesizes the monitor.
build(Hml, PropertyName) ->
  case analyze(Hml) of
    {ok, Subjects, SyntaxTree} ->
      io:format("~p~n", [SyntaxTree]),
      {ok, generate_code(PropertyName, synthesize_m(SyntaxTree), Subjects), Subjects};
    {error, Message} -> {error, Message}
  end.

%% Permits the compiler to be run from the command line.
%%compile([SourceFile, TargetFile]) -> compile(SourceFile, TargetFile).

%% Compiles the mHML formula in the specified source file, and outputs the
%% resulting monitor Erlang code in the specified target file.
compile(SourceFile, TargetFile) ->

  % Read whole file contents into property.
  Hml = case file:read_file(SourceFile) of
    {ok, Content} -> binary_to_list(Content);
    {error, Reason} -> throw({error, {build, Reason}})
  end,

  io:format("Processing file contents:~n ~s~n~n~n", [Hml]),

  PropertyName = filename:basename(TargetFile, ".erl"),

  case build(Hml, PropertyName) of
    {error, Message} ->
      io:format("An error occurred while building monitor: ~s.", [Message]);
    {ok, Code, _} ->
      util:code_to_file(TargetFile, Code),
      io:format("Written monitor module '~s' to file.~n", [PropertyName])
  end.

%% Generates the Erlang code from the specified monitor structure and subjects
%% into a module file with the name specified by module.
generate_code(Module, Monitor, Subjects) ->

%%  Dbg = case length(Subjects) of
%%    L when L > 0 ->
%%      log:trace_str("Subject(s) '~p' bound to values '~p' respectively.~n", [Subjects, util:stringify2(Subjects)]);
%%    _ -> ""
%%  end,

  {Ccs, Code} = Monitor,
  io:format("CCS: ~p~n", [Ccs]),

  Body = util:format("fun([~s]) -> ~s end.", [string:join(Subjects, ","), Code]),
  Prop = util:format("property() -> ~s", [Body]),
  util:format("-module(~s).~n-include(\"log.hrl\").~n-export([property/0]).~n~s", [Module, Prop]).


%% Generates the monitor CCS expression and its implementation as Erlang code.
synthesize_m(SyntaxTree) ->
  synthesize(SyntaxTree).

%% Generates the monitor for monitoring tt.
synthesize({ff}) ->
  {no, to_mon_fun("ff()")};

%% Generates the monitor for monitoring ff.
synthesize({tt}) ->
  {yes, to_mon_fun("tt()")};

%% Generates the monitor combining two formulae phi and psi.
synthesize({and_op, Phi, Psi}) ->
  case {synthesize(Phi), synthesize(Psi)} of
    {{Tag, Mon}, {yes, _}} -> {Tag, Mon};
    {{yes, _}, {Tag, Mon}} -> {Tag, Mon};
    {{PhiTag, PhiMon}, {PsiTag, PsiMon}} -> {ccs_par(PhiTag, PsiTag), to_mon_fun(util:format("and(~s,~s)", [PhiMon, PsiMon]))}
  end;

%% Generates the monitor combining two formulae phi and psi.
synthesize({or_op, Phi, Psi}) ->
  case {synthesize(Phi), synthesize(Psi)} of
    {{Tag, Mon}, {no, _}} -> {Tag, Mon};
    {{no, _}, {Tag, Mon}} -> {Tag, Mon};
    {{PhiTag, PhiMon}, {PsiTag, PsiMon}} -> {ccs_par(PhiTag, PsiTag), to_mon_fun(util:format("or(~s,~s)", [PhiMon, PsiMon]))}
  end;

synthesize({nec, Pat, Phi}) ->
  case synthesize(Phi) of
    {yes, _} -> {yes, to_mon_fun("tt()")};
    {Tag, Mon} ->
      Body = util:format("case Action of ~s -> ~s; _ -> ~s end", [pat_to_str(Pat), Mon, to_mon_fun("id()")]),
      Fun = util:format("fun(Action) -> ~s end", [Body]),
      {ccs_seq(nec, Tag), to_mon_fun(util:format("nec(~s)", [Fun]))}
  end;

synthesize({pos, Pat, Phi}) ->
  case synthesize(Phi) of
    {no, _} -> {no, to_mon_fun("ff()")};
    {Tag, Mon} ->
      Body = util:format("case Action of ~s -> ~s; _ -> ~s end", [pat_to_str(Pat), Mon, to_mon_fun("id()")]),
      Fun = util:format("fun(Action) -> ~s end", [Body]),
      {ccs_seq(pos, Tag), to_mon_fun(util:format("pos(~s)", [Fun]))}
  end;

%% Generates the monitor for the maximal fixpoint applied on formula phi.
synthesize({max, Var, Phi}) ->
  case synthesize(Phi) of
    {yes, _} -> {yes, to_mon_fun("tt()")};
    {var, _} -> {yes, to_mon_fun("tt()")};
    {Tag, Mon} -> {ccs_rec(Var, Tag), to_mon_fun(util:format("max(~p,~s)", [Var, Mon]))}
  end;

%% Generates the monitor for the minimal fixpoint applied on the formula phi.
synthesize({min, Var, Phi}) ->
  case synthesize(Phi) of
    {no, _} -> {no, to_mon_fun("ff()")};
    {var, _} -> {no, to_mon_fun("ff()")};
    {Tag, Mon} -> {ccs_rec(Var, Tag), to_mon_fun(util:format("min(~p,~s)", [Var, Mon]))}
  end;

%% Generates the monitor for the application of the variable var.
synthesize({var, Var}) ->
  {Var, to_mon_fun(util:format("var(~p)", [Var]))};

%% Generates the monitor for the guard with the specified expression choosing
%% to execute either formula phi or psi.
synthesize({guard, Expr, Phi, Psi}) ->
  {{_, PhiMon}, {_, PsiMon}} = {synthesize(Phi), synthesize(Psi)},
  {guard, to_mon_fun(util:format("guard(~s,~s,~s)", [util:str(Expr), PhiMon, PsiMon]))}.

%% Returns the list of unique subjects from the specified list of tokens,
%% ordered by the order of appearance.
get_subjects(Tokens) ->
  util:mapfoldl_unique(fun({subj, _, Val}) -> Val; (_) -> false end, Tokens).

%% Prefixes the specified function name with the monitor name.
to_mon_fun(Function) ->
  util:format("~s:mon_~s", [?MONITOR, Function]).

%% Returns the CCS representation of the sequential composition of the specified
%% two processes.
ccs_seq(Proc1, Proc2) -> list_to_atom(atom_to_list(Proc1) ++ "." ++ atom_to_list(Proc2)).

%% Returns the CCS representation of the parallel composition of the specified
%% two processes.
ccs_par(Proc1, Proc2) -> list_to_atom(atom_to_list(Proc1) ++ "|" ++ atom_to_list(Proc2)).

%% Returns the CCS representation of recursive calls on the specified variable
%% and process.
ccs_rec(Var, Proc) -> list_to_atom("rec" ++ atom_to_list(Var) ++ "(" ++ atom_to_list(Proc) ++ ")").

%% Converts the specified action pattern into its string representation in Erlang.
pat_to_str({Var, Event, Msg}) -> util:format("{~s,~s,~s}", [Event, Var, util:str(Msg)]).

%%%% TESTS %%%%
%util:expr_as_str("(a * (1 + h) < 5)\n=:=\nHello + 78 / [g,j,{h,k}]\n+\n{k,l} * ([y,{o,l,{k},[],[g,k]}] / +7 / (-9 + [1,2,3,[u,K,J,h,v]]))").
%f(Tokens), {ok, Tokens, _} = lexer:string("(a * (1 + h) < 5)\n=:=\nHello + 78 / [g,j,{h,k}]\n+\n{k,l} * ([y,{o,l,{k},[],[g,k]}] / +7 / (-9 + [1,2,3,[u,K,J,h,v]]))"), io:format("~p~n", [Tokens]), f(SyntaxTree), {ok, SyntaxTree} = parser:parse(Tokens), io:format("~p~n", [SyntaxTree]).

% Static vs. Dynamic test
%Free = fun(A) -> case A of B -> io:format("B = ~p~n", [B]); _X-> io:format("No match _X = ~p~n", [_X]) end end.
%Bound = fun() -> B = "Duncan", Free("Test") end.
%Bound2 = fun() -> B = "Duncan", A = "Test", case A of B -> io:format("B = ~p~n", [B]); _X-> io:format("No match _X = ~p~n", [_X]) end end.

%erl -run compiler compile "myform.txt" "b.erl" -run init stop -noshell

% Run plus_one server from command line - unmonitored.
%erl -pa ebin -eval "plus_one:start(eql)"

% Instrument the system using the make file.
%make instrument hml="/home/duncan/Dropbox/MSc/Development/Thesis/detecter-lite/priv/prop1.txt" app-bin-dir="/home/duncan/Dropbox/MSc/Development/Thesis/rv2016/ebin" MFA="{plus_one, start, [eql]}"

% Run the monitored system.
%erl -pa ebin -eval "launcher:start()"
