

Definitions.

% Digit regex.
DIGIT									= [0-9]

% Alphanumberic regex.
ALPHA									=	[a-zA-Z0-9_]

% Uppercase letter regex.
UPPER									=	[A-Z]

% Lowercase letter regex.
LOWER									=	[a-z]

% Uppercase letter and underscore regex.
UPPER_ 								= [A-Z_]

% Lowercase letter and underscore regex.
LOWER_ 								= [a-z_]

% Define the 'whitespace' regex.
WS  									= ([\000-\s]|%.*)

% Define Erlang input '?', output '!', call 'c' and return 'r' regex.
ACT_OP 								= (\!|\?|call|ret)

% Conditional operators regex.
COND_OP 							= (==|/=|=<|\<|>=|\>|=:=|=/=)
% COND_OP 							= (==|/=|=<|>=|=:=|=/=)

% Mathematical operators regex.
MATH_OP 							= (\+|\-|\*|/)

% Boolean operators regex.
BOOL_OP 							= (and|or|andalso|orelse|not)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Token definitions.                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Rules.

ff										: {token, {ff, TokenLine}}.
tt										: {token, {tt, TokenLine}}.
max										: {token, {max, TokenLine}}.
min										: {token, {min, TokenLine}}.
iff  									: {token, {iff, TokenLine}}.
implies								: {token, {implies, TokenLine}}.
else									: {token, {else, TokenLine}}.
&&										: {token, {and_op, TokenLine}}.
\|\|									: {token, {or_op, TokenLine}}.

% Define sHML action token.
{ACT_OP}							: {token, {action_op, TokenLine, list_to_atom(TokenChars)}}.

% Define Erlang integer token.
{DIGIT}+							: {token, {integer, TokenLine, list_to_integer(TokenChars)}}.

% Define Erlang float token.
{DIGIT}+\.{DIGIT}+ 		: {token, {float, TokenLine, list_to_float(TokenChars)}}.

% Define symbols.
[\\(),\[\]{}\|]   			: {token, {list_to_atom(TokenChars), TokenLine}}.

% Define Erlang conditional operator token.
{COND_OP}							: {token, {list_to_atom(TokenChars), TokenLine}}.

% Define Erlang mathematical operator token.
{MATH_OP}							: {token, {list_to_atom(TokenChars), TokenLine}}.

% Define Erlang boolean operator token.
{BOOL_OP}							:	{token, {list_to_atom(TokenChars), TokenLine}}.

% Define sHML fixpoint variable token.
{UPPER_}{ALPHA}*			: {token, {var, TokenLine, TokenChars}}.

% Define sHML action subject token.
@{UPPER_}{ALPHA}*			: {token, {subj, TokenLine, chop_hd(TokenChars)}}.

{LOWER}{LOWER_}*			: {token, {atom, TokenLine, list_to_atom(TokenChars)}}.

% Define Erlang atom token.
'{ALPHA}+'						: {token, {atom, TokenLine, list_to_atom(strip(TokenChars, TokenLen))}}.

% Define Erlang string token.
"{ALPHA}*"						: {token, {string, TokenLine, TokenChars}}.

% Whitespace and comment tokens.
{WS}+   							: skip_token.

Erlang code.

strip(TokenChars) -> string:strip(TokenChars, both).
strip(TokenChars,TokenLen) -> lists:sublist(TokenChars, 2, TokenLen - 2).
chop_hd(TokenChars) -> string:substr(TokenChars, 2).
