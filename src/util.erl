%%%-------------------------------------------------------------------
%%% @author duncan
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 23. Feb 2016 4:59 PM
%%%-------------------------------------------------------------------
-module(util).
-author("duncan").

-import(lists, [reverse/1, duplicate/2, flatten/1]).

%% API
%%-export([is_sys_process/1, fun_str/1]).
-compile(export_all).

-define(LINE_WIDTH, 500).

%%%% Determines whether the specified pid is a system process.
%%is_sys_process(Pid) when is_pid(Pid) ->
%%  lists:member(Pid, [group_leader() | [whereis(P) || P <- sys_processes()]]);
%%is_sys_process(Pid) ->
%%  lists:member(Pid, sys_processes()).
%%
%%%% Returns the list of processes considered to be Erlang system processes.
%%sys_processes() ->
%%  [code_server, user].

%% Returns the string representation of the specified fun.
fun_str(Fun) ->
  Info = erlang:fun_info(Fun),
  Module = proplists:get_value(module, Info),
  Name = atom_to_list(proplists:get_value(name, Info)),
  Arity = proplists:get_value(arity, Info),

  % Parse the fun name and obtain its constituents.
  {DefFun, Type, FunIndex} = parse_fun_name(Name),

  % Return a user-friendly formatted string containing all the fun details.
  lists:concat([Module, ":", DefFun, " -> ", Type, "/", Arity,
    " (fun index: ", FunIndex, ")"]).

%% Parses the function details from the specified stacktrace name.
parse_fun_name(Name) ->

  % Extract the constituents from the fun name; these are as follows:
  % - 'name/arity' of the function in which the fun is defined.
  % - fun
  % - the index of fun within the defining function.
  {match, [_, {Fus, Ful}, {Fs, Fl}, {Is, Il}]} =
    re:run(Name, "^-([a-zA-Z0-9_]+/[0-9]+)-(fun)-([0-9]+)-$"),

  % Extract sub-string from name list, according to the regex matches.
  DefFun = lists:sublist(Name, Fus + 1, Ful),
  Fun = lists:sublist(Name, Fs + 1, Fl),
  FunIndex = lists:sublist(Name, Is + 1, Il),
  {DefFun, Fun, FunIndex}.


find_first(Elem, [Elem | _]) -> Elem;
find_first(Elem, [_ | Tl]) -> find_first(Elem, Tl);
find_first(_, []) -> undefined.

contains(Val, List) -> find_first(Val, List) =/= undefined.

add_unique(Elem, List) ->
  case contains(Elem, List) of
    false -> reverse([Elem | List]);
    _ -> List
  end.

mapfoldl_unique(Fun, List) ->
  {_, Unique} = lists:mapfoldl(fun(Elem, Acc) ->
    case Fun(Elem) of
      false -> {1, Acc};
      Val ->
        case contains(Val, Acc) of
          false -> {1, [Val | Acc]};
          _ -> {1, Acc}
        end
    end
  end, [], List),
  reverse(Unique).

format(String) ->
  String.
format(String, Params) ->
  lists:flatten(io_lib:format(String, Params)).

encode_anons(Term) ->
  re:replace(Term, "([0-9\\[\\]()\\{\\},\\.\\000-\\s])_([0-9\\[\\]()\\{\\},\\.\\000-\\s])", "\\1'_'\\2", [global, {return, list}]).

decode_anons(Term) ->
  re:replace(Term, "([0-9\\[\\]()\\{\\},\\.\\000-\\s])'_'([0-9\\[\\]()\\{\\},\\.\\000-\\s])", "\\1_\\2", [global, {return, list}]).

%%pretty_print(Code) when is_list(Code) -> pretty_print(Code, 2).
%%
%%pretty_print(Code, Indent) when is_list(Code), is_integer(Indent), Indent >= 0 ->
%%  {ok, Tokens, _} = erl_scan:string(Code),
%%  {ok, SyntaxTree} = erl_parse:parse_form(Tokens),
%%  Pretty =
%%    erl_prettypr:format(SyntaxTree, [{paper, ?LINE_WIDTH}, {ribbon, ?LINE_WIDTH}]),
%%  re:replace(Pretty, "\\t", duplicate(Indent, " "), [global, {return, list}]).

stringify2(Term) -> stringify2(Term, false).

stringify2(Term, Quote) ->
  Str = io_lib:format("~p", [Term]),
  Unquoted = re:replace(Str, "\"(.*)\"", "\\1", [global, ungreedy, {return, list}]),
  Replaced = re:replace(Unquoted, "\\\\(.*)\\\\", "\"\\1\"", [global, ungreedy, {return, list}]),
  flatten(case Quote of
    true -> io_lib:format("\"~s\"", [Replaced]);
    false -> Replaced
  end).

escape_quote(String) ->
  flatten(re:replace(String, "\"(.*)\"", "`\\1`", [global, ungreedy, {return, list}])).

format_file(SourceFile) ->
  erl_tidy:file(SourceFile, [{paper, ?LINE_WIDTH}, {ribbon, ?LINE_WIDTH}, {backups, false}]).

parse_erl_expr(Expr) ->
  {ok, Scanned, _} = erl_scan:string(Expr ++ "."),
%%  io:format("Scanned: ~p.~n~n~n", [Scanned]),
  {ok, Parsed} = erl_parse:parse_exprs(Scanned),
%%  io:format("Parsed: ~p.~n", [Parsed]),
  {value, Value, _} = erl_eval:exprs(Parsed, erl_eval:new_bindings()),
  Value.
%%  erl_pp:exprs(Parsed).



%%expr_as_str(Expr) ->
%%  flatten(parse_erl_str).

str(SyntaxTree) ->
  flatten(erl_pp:exprs([SyntaxTree])).

str(SyntaxTree, Quote) ->
  flatten("\"" ++ strip_consecutive_quotes(flatten(erl_pp:exprs([SyntaxTree]))) ++ "\"").

as_list([{Category, Line, Symbol, Symbol2} | T], Line) ->
  {cons, Line, {Category, Line, Symbol, Symbol2}, as_list(T, Line)};
as_list([{Category, Line, Symbol} | T], Line) ->
  {cons, Line, {Category, Line, Symbol}, as_list(T, Line)};
as_list([{Category, Line} | T], Line) ->
  {cons, Line, {Category, Line}, as_list(T, Line)};
as_list([], Line) -> {nil, Line}.

%%as_list([{cons,1,{atom,1,a},{nil,1}}],1)


strip_consecutive_quotes(Str) ->
%%  re:replace(Str, "\\\"(.*)\\\"", "str(\\1)", [global, ungreedy, {return, list}]),
  Unquoted = re:replace(Str, "\"(.*)\"", "\\1", [global, ungreedy, {return, list}]),
  re:replace(Unquoted, "\\\\(.*)\\\\", "str\(\\1\)", [global, ungreedy, {return, list}]).


%% Returns a predicate that determines whether the character C ought to be
%% filtered out.
filter(C, Chars) ->
  fun(Keep) ->
    case lists:member(C, Chars) of
      true -> Keep;
      _ -> not Keep
    end
  end.

%% Removes occurrences of characters in Chars from the specified String.
strip_chars(String, Chars) ->
  lists:filter(fun(C) -> (filter(C, Chars))(false) end, String).

%% Keeps occurrences of characters in Chars from the specified String.
keep_chars(String, Chars) ->
  lists:filter(fun(C) -> (filter(C, Chars))(true) end, String).

str_to_file(Filename, String) ->
  filelib:ensure_dir(Filename),
  file:write_file(Filename, io_lib:fwrite("~s", [String])).

code_to_file(Filename, Code) ->
  str_to_file(Filename, Code),
  util:format_file(Filename).

file_to_str(Filename) ->
  {ok, Contents} = file:read_file(Filename),
  unicode:characters_to_list(Contents).