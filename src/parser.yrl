%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sHML parser syntax definition.                                               %
% author: Duncan                                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Nonterminals
formula shml chml
nec pos guard
item
bool_expr bool_exprs comp_expr math_expr
unary_op fact_op term_op comp_op
list tail
tuple.

Terminals
'(' ')' '[' ']' '{' '}' ',' '|' '\\'
not
'*' '/' and
'+' '-' or
'==' '/=' '=<' '<' '>=' '>' '=:=' '=/='
andalso orelse
ff tt and_op or_op max min action_op subj iff implies else
atom integer float string var.

Rootsymbol formula.

% Define operator precedence for sHML formulas and Erlang operators. operators
% are listed in terms of increasing precedence.
% Unary 100 formula.
Unary 250 shml.
% Unary 200 chml.

Left 200 and_op.
Left 200 or_op.

Left 300 iff.
Left 300 implies.
Left 400 else.
Left 400 orelse.
Left 500 andalso.


Unary 500 nec.
Unary 500 pos.
Left 600 comp_op.
Left 700 term_op.
Left 800 fact_op.
Unary 900 unary_op.

formula -> shml : '$1'.
formula -> chml : '$1'.

shml -> ff                                                    : {ff}.
shml -> tt                                                    : {tt}.
shml -> shml and_op shml                                      : {and_op, '$1', '$3'}.
shml -> nec shml                                              : {nec, '$1', '$2'}.
nec -> '[' subj action_op comp_expr ']'                       : {?symb('$2'), action_op_val(?symb('$3')), '$4'}.
nec -> '[' var action_op comp_expr ']'                        : {?symb('$2'), action_op_val(?symb('$3')), '$4'}.
shml -> atom                                                  : {var, ?symb('$1')}.
shml -> max '(' atom ',' shml ')'                             : {max, ?symb('$3'), '$5'}.
shml -> '(' shml ')'                                          : '$2'.


chml -> ff                                                    : {ff}.
chml -> tt                                                    : {tt}.
chml -> chml or_op chml                                       : {or_op, '$1', '$3'}.
chml -> pos chml                                              : {pos, '$1', '$2'}.
pos -> '/' subj action_op comp_expr '\\'                       : {?symb('$2'), action_op_val(?symb('$3')), '$4'}.
pos -> '/' var action_op comp_expr '\\'                        : {?symb('$2'), action_op_val(?symb('$3')), '$4'}.
chml -> atom                                                  : {var, ?symb('$1')}.
chml -> min '(' atom ',' chml ')'                             : {min, ?symb('$3'), '$5'}.
chml -> '(' chml ')'                                          : '$2'.

% max('X', max('Y', 'Y') \n'X')
% {max,1}, {'(',1}, {atom,1,'X'}, {',',1}, {max,1}, {'(',1}, {atom,1,'Y'}, {',',1}, {atom,1,'Y'}, {')',1}, {atom,2,'X'}, {')',2}


% Define the syntax for the formulas in the sub-logic sHML.
% formula -> ff                                                 : {ff}.
% formula -> tt                                                 : {tt}.
% formula -> formula and_op formula                             : {and_op, '$1', '$3'}.
% formula -> nec formula                                        : {nec, '$1', '$2'}.
% nec -> '[' subj action_op comp_expr ']'                      : {?symb('$2'), action_op_val(?symb('$3')), '$4'}.
% nec -> '[' var action_op comp_expr ']'                       : {?symb('$2'), action_op_val(?symb('$3')), '$4'}.
% formula -> atom                                               : {var, ?symb('$1')}.
% formula -> max '(' atom ',' formula ')'                       : {max, ?symb('$3'), '$5'}.
% formula -> '(' formula ')'                                    : '$2'.

shml -> guard                                              : '$1'.
guard -> iff '(' bool_expr ')' implies shml                : {guard, '$3', '$6', {tt}}.
guard -> iff '(' bool_expr ')' implies shml else shml   : {guard, '$3', '$6', '$8'}.

chml -> guard                                              : '$1'.
guard -> iff '(' bool_expr ')' implies chml                : {guard, '$3', '$6', {tt}}.
guard -> iff '(' bool_expr ')' implies chml else chml   : {guard, '$3', '$6', '$8'}.

% Define the syntax for guards; these extend HML formulas by adding
% 'if implies else' construct.
% formula -> guard                                              : '$1'.
% guard -> iff '(' bool_expr ')' implies formula                : {guard, '$3', '$6', {tt}}.
% guard -> iff '(' bool_expr ')' implies formula else formula   : {guard, '$3', '$6', '$8'}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Erlang expression definitions.                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define the syntax for boolean expressions; these are the basic expression
% types in Erlang from which represent any other Erlang expression.
bool_expr -> bool_expr orelse bool_expr               : {op, ?line('$2'), ?catg('$2'), '$1', '$3'}.
bool_expr -> bool_expr andalso bool_expr              : {op, ?line('$2'), ?catg('$2'), '$1', '$3'}.
bool_expr -> comp_expr                                    : '$1'.

% Define syntax for comparison expressions; these use the comparion operators
% defined below.
comp_expr -> math_expr comp_op math_expr                      : {op, ?line('$2'), ?catg('$2'), '$1', '$3'}.
comp_expr -> math_expr                                        : '$1'.

% Define syntax for mathematical expressions; these use the mathematical
% operators defined below. Unary operators have the highest precedence
% followed by factor operators, followed by term operators. Note that
% 'and' and 'or' are considered as mathematical operators.
math_expr -> math_expr term_op math_expr                      : {op, ?line('$2'), ?catg('$2'), '$1', '$3'}.
math_expr -> math_expr fact_op math_expr                      : {op, ?line('$2'), ?catg('$2'), '$1', '$3'}.
math_expr -> unary_op math_expr                               : {op, ?line('$1'), ?catg('$1'), '$2'}.
math_expr -> item                                             : '$1'.

% Define syntax for boolean expressions; these are simply single boolean
% expressions or lists of boolean expressions.
bool_exprs   -> bool_expr                                     : ['$1'].
bool_exprs   -> bool_expr ',' bool_exprs                      : ['$1' | '$3'].

% Define the unary operators; these have the highest precedence.
unary_op -> '+' : '$1'.
unary_op -> '-' : '$1'.
unary_op -> not : '$1'.

% Define factor operators; these have precedence lower than unary operators.
fact_op -> '/' : '$1'.
fact_op -> '*' : '$1'.
fact_op -> and : '$1'.

% Define term operators; these have the lowest precedence.
term_op -> '+' : '$1'.
term_op -> '-' : '$1'.
term_op -> or : '$1'.

% Define comparison operators, not used in mathematical epxressions.
comp_op -> '==' : '$1'.
comp_op -> '/=' : '$1'.
comp_op -> '=<' : '$1'.
comp_op -> '<' : '$1'.
comp_op -> '>=' : '$1'.
comp_op -> '>' : '$1'.
comp_op -> '=:=' : '$1'.
comp_op -> '=/=' : '$1'.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Erlang basic syntax definitions.                                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Erlang tuples.
tuple   -> '{' '}'                                            : {tuple, ?line('$1'), []}.
tuple    -> '{' bool_exprs '}'                                : {tuple, ?line('$1'), '$2'}.

% Erlang lists, including the cons operator.
list -> '[' ']'                                               : {nil,?line('$1')}.
list -> '[' bool_expr tail                                    : {cons,?line('$1'),'$2','$3'}.
tail -> ']'                                                   : {nil,?line('$1')}.
tail -> '|' bool_expr ']'                                     : '$2'.
tail -> ',' bool_expr tail                                    : {cons,?line('$2'),'$2','$3'}.

% Complex patterns.
item -> tuple                                                 : '$1'.
item -> list                                                  : '$1'.

% Erlang primitives and bracketed expressions.
item    -> var                                                : '$1'.
item    -> atom                                               : '$1'.
item    -> string                                             : '$1'.
item    -> integer                                            : '$1'.
item    -> float                                              : '$1'.
item -> '(' bool_expr ')'                                     : '$2'.

Erlang code.

% Macros to exract elements from tuples returned by the lexer.
-define(catg(Tuple), element(1, Tuple)).
-define(line(Tuple), element(2, Tuple)).
-define(symb(Tuple), element(3, Tuple)).

% Translate between lexer literal tokens to HML interaction operators.
action_op_val('?') -> recv;
action_op_val('!') -> send.
% action_op_val('c') -> call;
% action_op_val('r') -> rtrn.

%f(Tokens), {ok, Tokens, _} = lexer:string("/A ? b\\ff || /A ! f\\ff"), io:format("~p~n", [Tokens]), f(SyntaxTree), {ok, SyntaxTree} = parser:parse(Tokens), io:format("~p~n", [SyntaxTree]).
% {or_op,
%   {pos,
%     {"A",recv,
%       {atom,1,b}
%     },
%     {ff}
%   },
%   {pos,
%     {"A",send,
%       {atom,1,f}
%     },
%     {ff}
%   }
% }

%f(Tokens), {ok, Tokens, _} = lexer:string("/A ? b\\ff || /A ! f\\iff (x+1) implies tt else ff"), io:format("~p~n", [Tokens]), f(SyntaxTree), {ok, SyntaxTree} = parser:parse(Tokens), io:format("~p~n", [SyntaxTree]).
% {or_op,
%   {pos,
%     {"A",recv,{atom,1,b}},
%     {ff}
%   },
%   {pos,
%     {"A",send,{atom,1,f}},
%     {guard,
%       {op,1,'+',
%         {atom,1,x},
%         {integer,1,1}
%       },
%       {tt},
%       {ff}
%     }
%   }
% }

%f(Tokens), {ok, Tokens, _} = lexer:string("min('X', /A ? b\\ff || /A ! f\\iff (x+1) implies tt else 'X')"), io:format("~p~n", [Tokens]), f(SyntaxTree), {ok, SyntaxTree} = parser:parse(Tokens), io:format("~p~n", [SyntaxTree]).
% {min,'X',
%   {or_op,
%     {pos,
%       {"A",recv,{atom,1,b}},
%       {ff}
%     },
%     {pos,
%       {"A",send,{atom,1,f}},
%       {guard,
%         {op,1,'+',
%           {atom,1,x},
%           {integer,1,1}
%         },
%         {tt},
%         {var,'X'}
%       }
%     }
%   }
% }
