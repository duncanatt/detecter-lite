
# Project structure configuration.
SRC_DIR=src
LIB_DIR=include
BIN_DIR=ebin
TST_DIR=test

# Define the Erlang compiler and runner configuration.
ERLC=erlc -W
ERL=erl -boot start_clean

# Make does not offer a recursive wildcard function, so we create one ourselves.
rwildcard=$(wildcard $1$2) $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2))

# Define canned recipe for compiling Erlang '.erl' source files to '.beam'
# files.
define compile_erl
@echo "Compiling Erlang source file: $< to $(BIN_DIR)/$(notdir $@)"
erlc -W -DBASE_DIR="\"${CURDIR}\"" -I $(LIB_DIR) -o $(BIN_DIR) $<
endef

# Define canned recipe for processing YECC '.yrl' files to Erlang '.erl' source
# files.
define compile_yrl
@echo "Compiling YECC source file: $< to $@"
erlc -W -o $(BIN_DIR) $<
endef

define compile_xrl
@echo "Compiling LEEX source file: $< to $@"
erlc -W -o $(BIN_DIR) $<
endef

# Build the list of LEEX '.xrl' files, and convert the source filenames in
# $(SRC_DIR) to point to $(BIN_DIR) as Erlang '.erl' files.
XRL=$(patsubst $(SRC_DIR)/%.xrl,$(BIN_DIR)/%.erl,\
	$(wildcard $(SRC_DIR)/*.xrl))

# Build the list of YECC '.yrl' files, and convert the source filenames in
# $(SRC_DIR) to point to $(BIN_DIR) as Erlang '.erl' files.
YRL=$(patsubst $(SRC_DIR)/%.yrl,$(BIN_DIR)/%.erl,\
	$(wildcard $(SRC_DIR)/*.yrl))

# Build the list of all Erlang '.erl' source files to be compiled by recursing
# into all sub-directories under $(SRC_DIR).
SRC := $(call rwildcard,$(SRC_DIR)/,*.erl)

# Create the list of binary '.beam' files to be generated during the
# compilation process. This list is made up of the following:
# - Erlang source files written manually, found in $(SRC_DIR).
# - Erlang source files generated by LEEX, found in $(BIN_DIR).
# - Erlang source files generated by YECC, found in $(BIN_DIR).
BIN=$(SRC:$(SRC_DIR)/%.erl=$(BIN_DIR)/%.beam) \
	$(XRL:%.erl=%.beam) \
	$(YRL:%.erl=%.beam)

# Define 'all' explicit rule used to compile all Erlang source files. All
# source files written manually or generated by YECC and LEEX are also
# included in the compilation process.
# all: bin_dir $(XRL) $(YRL) $(BIN) #clean_yrl
all: compile
	@echo
	@echo ">-------------------------------<"
	@echo "> Build completed successfully! <"
	@echo ">-------------------------------<"

# Define 'compile' explict rule which takes care of the full build
# process. This process consists of the following:
# 1. Create the $(BIN_DIR) if non-existent.
# 2. Process LEEX files and emit Erlang '.erl' source files in $(BIN_DIR).
# 3. Process YECC files and emit Erlang '.erl' source files in $(BIN_DIR).
# 4. Compile all Erlang '.erl' files contained in $(SRC_DIR) and those
#    autogenerated in $(BIN_DIR).
# 5. Remove the Erlang '.erl' autogenerated files from $(BIN_DIR).
compile: bin_dir $(XRL) $(YRL) $(BIN) clean_gen_erl

# Define implicit pattern rule to compile LEEX '.xrl' source files read from
# $(BIN_DIR), to '.erl' files written in $(BIN_DIR).
$(BIN_DIR)/%.erl: $(SRC_DIR)/%.xrl
	$(compile_xrl)

# Define implicit pattern rule to compile YECC '.yrl' source files read from
# $(BIN_DIR), to '.erl' files written in $(BIN_DIR).
$(BIN_DIR)/%.erl: $(SRC_DIR)/%.yrl
	$(compile_yrl)

# Define implicit pattern rule to compile Erlang '.erl' source files read from
# $(SRC_DIR), to '.beam' files written in $(BIN_DIR).
$(BIN_DIR)/%.beam: $(SRC_DIR)/%.erl
	$(compile_erl)

# Define implicit pattern rule to compile Erlang '.erl' source files read from
# $(BIN_DIR), to '.beam' files written in $(BIN_DIR).
$(BIN_DIR)/%.beam: $(BIN_DIR)/%.erl
	$(compile_erl)

run: compile
	$(ERL) -pa ebin test -s load_mods start $(BIN:$(BIN_DIR)/%.beam=%)

# Define 'docs' explicit rule which takes care of generating the HTML
# documentation from the type and document information contained within these
# '.erl' source files. The result is placed in the docs directory.
docs:
	erl -noshell -run edoc_run application "'$(APP_NAME)'" \
		'"."' '[{def,{vsn,"$(VSN)"}}]'

# Define phony targets so that these target names do not conflict with files
# bearing the same name.
.PHONY: bin_dir clean_gen_erl clean

# Creates the $(BIN_DIR) if it does not exist.
bin_dir:
	@mkdir -p $(BIN_DIR)

# Removes Erlang '.erl' files autogenerated by LEEX or YECC from $(BIN_DIR).
clean_gen_erl:
	@rm -fd $(BIN_DIR)/*.erl

# Removes all '.beam' files from $(BIN_DIR), and finally deletes the directory
# as well.
clean:
	@echo "Cleaning directory [$(BIN_DIR)]"
	@rm -rf $(BIN_DIR)
	@rm -fd erl_crash.dump
	@echo
	@echo ">-------------------------------<"
	@echo "> Clean completed successfully! <"
	@echo ">-------------------------------<"

# Creates the standard Erlang directory structure.
init:
	@echo "Creating Erlang directory structure [$(SRC_DIR) $(LIB_DIR) $(TST_DIR)]"
	@mkdir -p $(SRC_DIR)
	@mkdir -p $(LIB_DIR)
	@mkdir -p $(TST_DIR)
	@echo
	@echo ">---------------------------------<"
	@echo "> Structure created successfully! <"
	@echo ">---------------------------------<"

# Instruments the target system by generating the monitors and supporting files
# which are then compiled and moved to the target system directory.
# The following command line args are required:
# hml: The absolute path name of the formula file.
# app-bin-dir: The absolute path name of the app binaries directory.
# MFA: The MFA starting the application to be instrumented.
instrument: compile
	erl -pa ${BIN_DIR} -run detecter instrument \
		"${hml}" "${app-bin-dir}" "${MFA}" -run init stop -noshell

.NOTPARALLEL:
