%%%-------------------------------------------------------------------
%%% @author duncan
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 31. Mar 2016 22:06
%%%-------------------------------------------------------------------
-module(plus_one).
-author("duncan").

-include("macros.hrl").
-include("log.hrl").

-export([start/1, stop/0, request/1]).

%% Starts the plus one server using the specified mode. The mode with which the
%% server is started alters the behavior of the server, and is used to drive the
%% testing process.
start(Mode) ->
  register(plus_one, spawn(fun() -> loop(1, Mode) end)),
  ?INFO("Started PLUS ONE server with initial value '~p' and mode '~p'.~n", [0, Mode]).

%% Stops the plus one server.
stop() ->
  plus_one ! {stop, normal},
  unregister(plus_one).

%% Sends a request to the plus one server.
request(Int) ->
  plus_one ! {request, self(), Int},
  receive
    {result, Result} -> Result;
    _ -> unknown_response
  end.

%% Services incoming client requests.
loop(Cnt, Mode) ->
  receive
    {request, Pid, Value} ->
      ?INFO("Received request with value '~p'.~n", [Value]),

      % Handles and replies to client requests depending on the mode the
      % plus one server was started with.
      Result = handle_resp(Cnt, Mode, Value, Pid),
      ?INFO("Sending response with value '~p', Current cnt '~p'.~n", [Result, Cnt]),

      % Rerun server loop.
      loop(Cnt + 1, Mode);
    {stop, _Reason} ->
      ?INFO("Stopped PLUS ONE server with ending value '~p'.~n", [Cnt]),
      stop;
    _R -> ?INFO("Unknown request '~p'...send a correct request.~n", [_R]),

      % Rerun server loop, but don't increment request count.
      loop(Cnt, Mode)
  end.

%% Services the client requests based on the specified value of mode.
handle_resp(Cnt, Mode, Value, Pid) ->
  case Mode of
    lim ->
      case Cnt of
        Lim when Lim > 100 -> Pid ! {error, limit_exceeded};
        _ -> Pid ! get_result(Value)
      end;
    fin ->
      Tries = lists:seq(1, get_rnd(10)),
      lists:foreach(fun(T) ->
        Pid ! {wait, list_to_atom(integer_to_list(T) ++ "/" ++ integer_to_list(length(Tries)))}
      end, Tries),
      Pid ! get_result(Value);
    rep ->
      Tries = lists:seq(1, get_rnd(5)),
      lists:foreach(fun(_) ->
        Pid ! get_result(Value)
      end, Tries),
      Pid ! get_result(Value);
    eql -> Pid ! {result, Value};
    _ -> Pid ! get_result(Value)
  end.

%% Returns the standard result to the client.
get_result(Value) -> {result, Value + 1}.

%% Generates a random number. The seed is re-initialized each time the function
%% is invoked.
get_rnd(Range) -> random:seed(now()), random:uniform(Range).

