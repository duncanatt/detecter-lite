%%%-------------------------------------------------------------------
%%% @author duncan
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 31. Mar 2016 22:06
%%%-------------------------------------------------------------------
-module(mhml_extended_tests).
-author("duncan").

-include("macros.hrl").

% Define temp, include directories.
-define(TEMP_DIR, "test").
-define(INCLUDE_DIR, "include").

% Define default property name.
-define(PROP, "prop").

-define(CLIENT, client).
-define(SERVER, plus_one).

% Include EUnit.
-include_lib("eunit/include/eunit.hrl").

% Imports.
-import(compiler, [build/2, compile/2]).

% S: The plus one server cannot receive a request with a negative value.
% max('X', [Server ? {request, _, Request}][Client ! _] iff (Request < 0) implies ff else 'X')

% S: The plus one server's response cannot be equal to the request sent to it.
% max('X', [Server ? {request, _, Request}][Client ! {response, Response}] iff (Request =:= Response) implies ff else 'X')

% S: The plus one server request count cannot reach 1000.
% max('X', [Server ? {request, _, Request}][Client ! _] iff (Request =:= 100) implies ff else 'X')
% max('X', [Server ? {request, _, 1000}][Client ! {result, _}]ff && [Server ? {request, _, _}][Client ! {result, _}] 'X')


% S: The plus one server's response value must be equal to the request value + 1.
% min('X', /Server ? {request, _, Request}\\/Client ! Response\\ iff (Response =:= Request + 1) implies 'X' else ff )


% CS: The plus one server's response value must reach 100.
% min('X', /Server ? {request, _, 100}\\/Client ! {result, _}\tt || [Server ? {request, _, _}][Client ! {result, _}] 'X')

% CS: Upon a sending a request to the plus one server, the client receives a response.
% min('X', /Server ? {request, Client, _}\\ min('Y', /Client ! {result, _}\tt || 'Y') 'X')
% Note in this case the Client in the request *must* be the one in the response





chml_test_() -> {timeout, 600, {foreachx,
  fun({Mode, Hml}) ->

    % Compile and load the module resulting from the synthesis of monitor
    % corresponding to HML.
    SpecInfo = {Prop, {_, _, _}} = build_and_load_spec(Hml),

    % Start mock server in tandem with monitor generated for the specified HML,
    % and for the specified subject.
    MonitorPid = main_mon:start({plus_one, start, [Mode]}, apply(Prop, property, []), []),

    % Wait for system to start.
    timer:sleep(1000),

    {config, SpecInfo, MonitorPid}
  end,
  fun(_, {config, SpecInfo, _}) ->

    % Stop system.
    ?SERVER:stop(),

    % Wait for system to stop.
    timer:sleep(1000),

    % Assert that system has been stopped.
    ?assertEqual(undefined, whereis(?SERVER)),

    % Unregister test client process name.
    %unregister(?CLIENT),

    % Assert that system has been stopped.
    ?assertEqual(undefined, whereis(?CLIENT)),

    % Clean up property produced files.
    clean_spec(SpecInfo)
  end,
  [
    {{incr, "max('X', [Server ? {request, _, Request}][Client ! _] iff (Request < 0) implies ff else 'X')"},
%%    {{incr, "max('X', [Server ? {request, _, Request}][Client ! _] && [Server ? {}][]'X')"},
      fun(_, {config, _, MonitorPid}) ->
        {"S: The plus one server cannot receive a request with a negative value", ?_test(begin

          % Register client as this will be used by the monitor.
          register(?CLIENT, self()),

          % Generate a list of random number between -1 and 8, and request the plus_one
          % server.
          RandomList = [random:uniform(10) - 2 || _ <- lists:seq(1, 100)],
          lists:foreach(fun ?SERVER:request/1, RandomList),

          % Wait for monitor to catch up.
          timer:sleep(1000),

          % Assert that monitor process is stopped.
          ?assertNot(is_process_alive(MonitorPid))
        end)}
      end}
    ,
%%    {{eql, "max('X', [Server ? {request, Client, Request}][Client ! {result, Result}] iff (Request =:= Result) implies ff else 'X')"},
    {{eql, "max('X', [Server ? {request, Client, Request}][Client ! {result, Request}] ff && [Server ? {request, Client, Request}][Client ! {result, Result}] 'X')"},
      fun(_, {config, _, MonitorPid}) ->
        {"S: The plus one server's response cannot be equal to the request sent to it", ?_test(begin

          % Register client as this will be used by the monitor.
          register(?CLIENT, self()),

          % Perform a simple request.
          ?SERVER:request(4),

          % Wait for monitor to catch up.
          timer:sleep(1000),

          % Assert that monitor process is stopped.
          ?assertNot(is_process_alive(MonitorPid))
        end)}
      end}
    ,
    {{incr, "max('X', [Server ? {request, _, 100}][Client ! {result, _}]ff && [Server ? {request, _, _}][Client ! {result, _}] 'X')"},
      fun(_, {config, _, MonitorPid}) ->
        {timeout, 60, {"S: The plus one server request count cannot reach 100 (1)", ?_test(begin

          % Register client as this will be used by the monitor.
          register(?CLIENT, self()),

          % Generate a list of random number between -1 and 8, and request the plus_one
          % server.
          SeqList = lists:seq(1, 1000),
          lists:foreach(fun ?SERVER:request/1, SeqList),

          % Wait for monitor to catch up.
          timer:sleep(1000),

          % Assert that monitor process is stopped.
          ?assertNot(is_process_alive(MonitorPid))
        end)}}
      end}
    ,
    {{incr, "max('X', [Server ? {request, _, Request}][Client ! _] iff (Request =:= 100) implies ff else 'X')"},
      fun(_, {config, _, MonitorPid}) ->
        {timeout, 60, {"S: The plus one server request count cannot reach 100 (2)", ?_test(begin

          % Register client as this will be used by the monitor.
          register(?CLIENT, self()),

          % Generate a list of random number between -1 and 8, and request the plus_one
          % server.
          SeqList = lists:seq(1, 1000),
          lists:foreach(fun ?SERVER:request/1, SeqList),

          % Wait for monitor to catch up.
          timer:sleep(1000),

          % Assert that monitor process is stopped.
          ?assertNot(is_process_alive(MonitorPid))
        end)}}
      end}
    ,
    %%{{incr, "max('X', [Server ? {request, _, 100}][Client ! {result, _}]tt && [Server ? {request, _, _}][Client ! {result, _}] 'X')"},
    {{incr, "min('X', /Server ? {request, _, 100}\\/Client ! {result, _}\\tt || /Server ? {request, _, _}\\/Client ! {result, _}\\ 'X')"},
      fun(_, {config, _, MonitorPid}) ->
        {timeout, 60, {"CS: The plus one server's response value must reach 100", ?_test(begin

          % Register client as this will be used by the monitor.
          register(?CLIENT, self()),

          % Generate a list of random number between -1 and 8, and request the plus_one
          % server.
          SeqList = lists:seq(1, 100),
          lists:foreach(fun ?SERVER:request/1, SeqList),

          % Wait for monitor to catch up.
          timer:sleep(1000),

          % Assert that monitor process is stopped.
          ?assertNot(is_process_alive(MonitorPid))
        end)}}
      end}
  ,
  %%{{err, "/Server ? {request, Client, _}\\ min('Y', /Client ! {result, _}\\tt || /Client ! _\\'Y')"},
  {{err, "/Server ? {request, Client, _}\\ min('Y', /Client ! _\\'Y' || /Client ! {result, _}\\tt)"},
  %%{{err, "min('X', /Server ? {request, Client, _}\\ min('Y', /Client ! _\\ 'Y' || /Client ! {result, _}\\ tt) || /Z ? _\\ 'X')"},
    fun(_, {config, _, MonitorPid}) ->
      {timeout, 60, {"CS: Upon a sending a request to the plus one server, the client eventually receives a correct response", ?_test(begin

        % Register client as this will be used by the monitor.
        register(?CLIENT, self()),

        % Perform a simple request.
        ?SERVER:request(1),

        % Wait for monitor to catch up.
        timer:sleep(2000),

        % Assert that monitor process is stopped.
        ?assertNot(is_process_alive(MonitorPid))
      end)}}
    end}
    ,
  {{none, "tt"},
    fun(_, {config, _, MonitorPid}) ->
      {timeout, 60, {"Always violated", ?_test(begin

        % Register client as this will be used by the monitor.
        register(?CLIENT, self()),

        % Perform a simple request.
        ?SERVER:request(1),

        % Wait for monitor to catch up.
        timer:sleep(2000),

        % Assert that monitor process is stopped.
        ?assertNot(is_process_alive(MonitorPid))
      end)}}
    end}
    ,
    {{none, "tt"},
      fun(_, {config, _, MonitorPid}) ->
        {timeout, 60, {"Always satisfied", ?_test(begin

          % Register client as this will be used by the monitor.
          register(?CLIENT, self()),

          % Perform a simple request.
          ?SERVER:request(1),

          % Wait for monitor to catch up.
          timer:sleep(2000),

          % Assert that monitor process is stopped.
          ?assertNot(is_process_alive(MonitorPid))
        end)}}
      end}
  ]}}.


%% Writes the specified Hml to file.
spec_to_file(HmlFile, Hml) ->
  file:write_file(HmlFile, io_lib:fwrite("~s", [Hml])).

%% Returns the tuple of file information.
get_prop_file_info() ->
  Files = [P ++ E || P <- [filename:join(?TEMP_DIR, ?PROP)],
    E <- [".txt", ".erl", ".beam"]],
  {list_to_atom(?PROP), list_to_tuple(Files)}.

%% Generates, compiles and dynamically loads the monitor from the specified Hml,
build_and_load_spec(Hml) ->

  % Compute file paths based on the value of the property. These will be used
  % to save the specification file, monitor property .erl file, and the
  % compiled property .beam file.
  Info = {Prop, {SpecFile, PropFile, _}} = get_prop_file_info(),

  % Set .beam load paths from which the compiled monitor file will be
  % loaded.
  code:add_path(?TEMP_DIR),

  % Write HML property specification to file.
  spec_to_file(SpecFile, Hml),

  % Generate monitor from the saved HML specification.
  compile(SpecFile, PropFile),

  % Compile and load the property .erl file just generated; this will be
  % used to test the monitor on the mock system.
  compile:file(PropFile, [{outdir, ?TEMP_DIR}, {d, debug}, {i, ?INCLUDE_DIR}]),
  code:load_file(Prop),

  % Return information on the files just created.
  Info.

%% Deletes the generates specification file, Erlang and beam file.
clean_spec({Prop, {SpecFile, PropFile, BeamFile}}) ->

  % Cleanup file generated earlier in the setup.
  file:delete(SpecFile),
  file:delete(PropFile),
  file:delete(BeamFile),

  % Unload monitor module.
  code:purge(Prop).



%% Questions to ask to Adrian:
%% 1. If we have a sHML/cHML, 2 formulae with and, like the example above, is
%% it ok if one stops and the other keeps going. Seems to be because of the
%% backlog, as the processes forked by the AND are still running. Is this normal?
%% In my opinion, I guess so.
%%
%% 2. What if we have [A][B]tt...what should happen? In other words, in practical
%% terms and in our implementation, what is the difference between
%% max('X', [Server ? {request, _, 100}][Client ! {result, _}]tt && [Server ? {request, _, _}][Client ! {result, _}] 'X') AND
%% min('X', /Server ? {request, _, 100}\\/Client ! {result, _}\\tt || /Server ? {request, _, _}\\/Client ! {result, _}\\ 'X')
%%
%% 3. When we encounter a violation, we stop the monitor. Do we do the same for satisfaction?
%%
%% 4. S: The plus one server's response value must be equal to the request value + 1. - is this a co-safety property?
%% EG is the below valid? Since it recurses on the 'good behaviour' not the bad, .i.e. it expects the ff.
%% What about efficiency of these properties in terms of continuous unfolding?
%% min('X', /Server ? {request, _, Request}\\/Client ! Response\\ iff (Response =:= Request + 1) implies 'X' else ff )
%%
%% 5. Does recursing indefinitely like the last property reduce itself to busy waiting?
%% i.e: /Server ? {request, Client, _}\\ min('Y', /Client ! {result, _}\\tt || /Client ! _\\'Y') VS
%% /Server ? {request, Client, _}\\ min('Y', /Client ! {result, _}\\tt || 'Y') (show ian's paper)
%% What about..."from the many request the server recieves, one must be of a certain form, and eventually
%% a reply must be sent to the client (see the last formula above with 2 min X and Y.
%%
%% 6. HML with the max/min...is it expressive as CTL (AE) G,F,U,X?
%%
%% 7. Should we kill the monitor when we detect violation or satisfaction?
%%
%% 8. When them monitor with one logic, say LTL with modified semantics, and they
%% they have a formula which perform using MC and RV. Do they have to use different
%% semantics for LTL in MC, and the same LTL syntax in RV?
%%
%% 9. What defines a modal logic? Just the [] and <>. What does it mean to be modal?
%% the fact that it deals with time? In the Aceto book, they say HML is modal.
%% Is LTL modal?
%%
%% 10. Is there such a thing as the monitor being efficient? For example, what if
%% we have a monitor that does busy waiting?
%%
%% 11. What if we have something like this [a][b] if(some_var==1) implies tt else 'X'.
%% In your paper you mention that [a]tt causes trouble, could this happen in our mHML case?
%%
%% 12. What if we have just tt in sHML..it just defaults to true and means satisfaction?
%%
%% 13. Why do we need a branching time logic like HML when we are reasoning on just
%% a single linear trace in RV. Or am I wrong?
%%
%% 14. About max/min: The max/min fixpoint is never reached by recHML, but we use
%% its recursive properties only, just as a loop? Or we do intend to reach a final
%% solution?
%%
%% 15. Is maxX and minX true and false respectively because one is the lowest upper
%% bound and the other, the highest lower bound of our formulae?
%%
%% 16. When you say monitor stopped, we mean the sub-monitor, not the main one.
%%
%% 17. In the monitor synthesis, are we expected to reduce maxX to true and minX to false?
%% If yes, do they sort of need to be reflected in the paper's monitor synthesis? Also
%% the times operator, does that need to change too.
%%
%% 18. Why did you use the plus operator in your paper, not the times?
%%
%% 19. What is the meaning of just tt in cHML and ff in sHML. What about tt in sHML and ff in cHML?
