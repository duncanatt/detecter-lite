%%%-------------------------------------------------------------------
%%% @author duncan
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 22. Apr 2016 14:59
%%%-------------------------------------------------------------------
-module(mhml_tests).
-author("duncan").

% Define temp, include directories.
-define(TEMP_DIR, "test").
-define(INCLUDE_DIR, "include").

% Define default property name.
-define(PROP, "prop").

-define(CLIENT, client).
-define(SERVER, plus_one).

% Include EUnit.
-include_lib("eunit/include/eunit.hrl").

chml_test_() -> {timeout, 600, {foreachx,
  fun({Mode, Hml}) ->

    % Compile and load the module resulting from the synthesis of monitor
    % corresponding to HML.
    SpecInfo = {Prop, {_, _, _}} = build_and_load_spec(Hml),

    % Start mock server in tandem with monitor generated for the specified HML,
    % and for the specified subject.
    MonitorPid = main_mon:start({?SERVER, start, [Mode]}, apply(Prop, property, []), []),

    % Wait for system to start.
    timer:sleep(1000),

    {config, SpecInfo, MonitorPid}
  end,
  fun(_, {config, SpecInfo, _}) ->

    % Stop system.
    ?SERVER:stop(),

    % Wait for system to stop.
    timer:sleep(2000),

    % Assert that system has been stopped.
    ?assertEqual(undefined, whereis(?SERVER)),

    % Unregister test client process name.
    %unregister(?CLIENT),

    % Assert that system has been stopped.
    ?assertEqual(undefined, whereis(?CLIENT)),

    % Clean up property produced files.
    clean_spec(SpecInfo)
  end,
  [
    {{lim, "max('X',
              [Server ? {request, _, _}][Client ! {error, limit_exceeded}]ff
              &&
              [Server ? {request, _, _}][Client ! {result, _}] 'X')"},
      fun(_, {config, _, MonitorPid}) ->
        {timeout, 60, {"Safety: The server request count cannot exceed 100", ?_test(begin

        % Register client as this will be used by the monitor.
          register(?CLIENT, self()),

          % Generate a sequential list of numbers to send to the server.
          SeqList = lists:seq(1, 1000),
          lists:foreach(fun ?SERVER:request/1, SeqList),

          % Wait for monitor to catch up.
          timer:sleep(2000),

          % Assert that monitor process is stopped.
          ?assertNot(is_process_alive(MonitorPid))
        end)}}
      end},
    {{rep, "max('X',
              [Server ? {request, _, _}][Client ! {result, _}][Client ! {result, _}]ff
              &&
              [Server ? {request, _, _}][Client ! {result, _}] 'X')"},
      fun(_, {config, _, MonitorPid}) ->
        {timeout, 60, {"Safety: The client cannot receive duplicate requests from the server", ?_test(begin

        % Register client as this will be used by the monitor.
          register(?CLIENT, self()),

          % Perform a simple request.
          ?SERVER:request(1),

          % Wait for monitor to catch up.
          timer:sleep(2000),

          % Assert that monitor process is stopped.
          ?assertNot(is_process_alive(MonitorPid))
        end)}}
      end},
    {{eql, "max('X',
              [Server ? {request, Client, Request}][Client ! {result, Request}] ff
              &&
              [Server ? {request, Client, Request}][Client ! {result, Result}] 'X')"},
      fun(_, {config, _, MonitorPid}) ->
        {"Safety: The server's response cannot be equal to the request sent to it", ?_test(begin

        % Register client as this will be used by the monitor.
          register(?CLIENT, self()),

          % Perform a simple request.
          ?SERVER:request(4),

          % Wait for monitor to catch up.
          timer:sleep(1000),

          % Assert that monitor process is stopped.
          ?assertNot(is_process_alive(MonitorPid))
        end)}
      end},
    {{lim, "min('X',
              /Server ? {request, _, _}\\/Client ! {error, limit_exceeded}\\tt
              ||
              /Server ? {request, _, _}\\/Client ! {result, _}\\ 'X')"},
      fun(_, {config, _, MonitorPid}) ->
        {timeout, 60, {"Co-safety: The server eventually processes 100 requests", ?_test(begin

        % Register client as this will be used by the monitor.
          register(?CLIENT, self()),

          % Generate a sequential list of numbers to send to the server.
          SeqList = lists:seq(1, 1000),
          lists:foreach(fun ?SERVER:request/1, SeqList),

          % Wait for monitor to catch up.
          timer:sleep(2000),

          % Assert that monitor process is stopped.
          ?assertNot(is_process_alive(MonitorPid))
        end)}}
      end},
    {{fin, "/Server ? {request, Client, _}\\
              min('Y', /Client ! {wait, _}\\'Y' || /Client ! {result, _}\\tt)"},
      fun(_, {config, _, MonitorPid}) ->
        {timeout, 60, {"Co-safety: When a sending a request to the server, the client eventually receives the correct result", ?_test(begin

        % Register client as this will be used by the monitor.
          register(?CLIENT, self()),

          % Perform a simple request.
          ?SERVER:request(1),

          % Wait for monitor to catch up.
          timer:sleep(2000),

          % Assert that monitor process is stopped.
          ?assertNot(is_process_alive(MonitorPid))
        end)}}
      end},
    {{none, "ff"},
      fun(_, {config, _, MonitorPid}) ->
        {timeout, 60, {"Always violated", ?_test(begin

        % Register client as this will be used by the monitor.
          register(?CLIENT, self()),

          % Perform a simple request.
          ?SERVER:request(1),

          % Wait for monitor to catch up.
          timer:sleep(2000),

          % Assert that monitor process is stopped.
          ?assertNot(is_process_alive(MonitorPid))
        end)}}
      end},
    {{none, "tt"},
      fun(_, {config, _, MonitorPid}) ->
        {timeout, 60, {"Always satisfied", ?_test(begin

        % Register client as this will be used by the monitor.
          register(?CLIENT, self()),

          % Perform a simple request.
          ?SERVER:request(1),

          % Wait for monitor to catch up.
          timer:sleep(2000),

          % Assert that monitor process is stopped.
          ?assertNot(is_process_alive(MonitorPid))
        end)}}
      end}
  ]}}.


%% Writes the specified Hml to file.
spec_to_file(HmlFile, Hml) ->
  file:write_file(HmlFile, io_lib:fwrite("~s", [Hml])).

%% Returns the tuple of file information.
get_prop_file_info() ->
  Files = [P ++ E || P <- [filename:join(?TEMP_DIR, ?PROP)],
    E <- [".txt", ".erl", ".beam"]],
  {list_to_atom(?PROP), list_to_tuple(Files)}.

%% Generates, compiles and dynamically loads the monitor from the specified Hml,
build_and_load_spec(Hml) ->

  % Compute file paths based on the value of the property. These will be used
  % to save the specification file, monitor property .erl file, and the
  % compiled property .beam file.
  Info = {Prop, {SpecFile, PropFile, _}} = get_prop_file_info(),

  % Set .beam load paths from which the compiled monitor file will be
  % loaded.
  code:add_path(?TEMP_DIR),

  % Write HML property specification to file.
  spec_to_file(SpecFile, Hml),

  % Generate monitor from the saved HML specification.
  compiler:compile(SpecFile, PropFile),

  % Compile and load the property .erl file just generated; this will be
  % used to test the monitor on the mock system.
  compile:file(PropFile, [{outdir, ?TEMP_DIR}, {d, debug}, {i, ?INCLUDE_DIR}]),
  code:load_file(Prop),

  % Return information on the files just created.
  Info.

%% Deletes the generates specification file, Erlang and beam file.
clean_spec({Prop, {SpecFile, PropFile, BeamFile}}) ->

  % Cleanup file generated earlier in the setup.
  file:delete(SpecFile),
  file:delete(PropFile),
  file:delete(BeamFile),

  % Unload monitor module.
  code:purge(Prop).