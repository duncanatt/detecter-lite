%%%-------------------------------------------------------------------
%%% @author duncan
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 21. Apr 2016 18:38
%%%-------------------------------------------------------------------
-module(compiler_tests).
-author("duncan").

% Include EUnit.
-include_lib("eunit/include/eunit.hrl").

%% Tests the mHML compiler.
mHml_test_() -> {foreachx,
  fun(Hml) ->

    % Create syntax tree from Hml.
    {ok, _, SyntaxTree} = compiler:analyze(Hml),

    % Generate Erlang code.
    compiler:synthesize_m(SyntaxTree)
  end,
  fun(_, Code) ->
    ?debugFmt("~nGenerated: ~p.~n", [Code])
  end,
  [
    {"ff",
      fun(_, {Ccs, Code}) ->
        {"mHml: ff only", ?_test(begin

        % The expected CCS and monitor code.
          ExpCcs = 'no',
          ExpCode = "formula:mon_ff()",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"tt",
      fun(_, {Ccs, Code}) ->
        {"mHml: tt only", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'yes',
          ExpCode = "formula:mon_tt()",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"'X'",
      fun(_, {Ccs, Code}) ->
        {"mHml: recursion variable 'X'", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'X',
          ExpCode = "formula:mon_var('X')",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"[A?a]ff",
      fun(_, {Ccs, Code}) ->
        {"sHml: necessity ending with ff", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'nec.no',
          ExpCode = "
            formula:mon_nec(
              fun(Action) ->
                case Action of
                  {recv, A, a} ->
                    formula:mon_ff();
                  _ -> formula:mon_id()
                end
              end
            )",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"[A?a][B!b]ff",
      fun(_, {Ccs, Code}) ->
        {"sHml: multiple necessities ending with ff", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'nec.nec.no',
          ExpCode = "
            formula:mon_nec(
              fun(Action) ->
                case Action of
                  {recv, A, a} ->
                    formula:mon_nec(
                      fun(Action) ->
                        case Action of
                          {send, B, b} ->
                            formula:mon_ff();
                          _ -> formula:mon_id()
                        end
                      end
                    );
                  _ -> formula:mon_id()
                end
              end
            )",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"[A?a][B!b]tt",
      fun(_, {Ccs, Code}) ->
        {"sHml: multiple necessities ending with tt", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'yes',
          ExpCode = "formula:mon_tt()",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"[A?a]ff && [B?b]tt",
      fun(_, {Ccs, Code}) ->
        {"sHml: conjuction of necessities with psi equating to true", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'nec.no',
          ExpCode = "
            formula:mon_nec(
              fun(Action) ->
                case Action of
                  {recv, A, a} ->
                    formula:mon_ff();
                  _ -> formula:mon_id()
                end
              end
            )",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"[A?a]tt && [B?b]ff",
      fun(_, {Ccs, Code}) ->
        {"sHml: conjuction of necessities with phi equating to true", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'nec.no',
          ExpCode = "
            formula:mon_nec(
              fun(Action) ->
                case Action of
                  {recv, B, b} ->
                    formula:mon_ff();
                  _ -> formula:mon_id()
                end
              end
            )",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"[A?a]ff && [B?b]ff",
      fun(_, {Ccs, Code}) ->
        {"sHml: conjuction of necessities phi and psi", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'nec.no|nec.no',
          ExpCode = "
            formula:mon_and(
              formula:mon_nec(
                fun(Action) ->
                  case Action of
                    {recv, A, a} ->
                      formula:mon_ff();
                    _ -> formula:mon_id()
                  end
                end
              ),
              formula:mon_nec(
                fun(Action) ->
                  case Action of
                    {recv, B, b} ->
                      formula:mon_ff();
                    _ -> formula:mon_id()
                  end
                end
              ))",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"max('X', [A?a]ff)",
      fun(_, {Ccs, Code}) ->
        {"sHml: max fixpoint with necessity ending with ff", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'recX(nec.no)',
          ExpCode = "
            formula:mon_max('X',
              formula:mon_nec(
              fun(Action) ->
                case Action of
                  {recv, A, a} ->
                    formula:mon_ff();
                  _ -> formula:mon_id()
                end
              end
            ))",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"max('X', [A?a]'X')",
      fun(_, {Ccs, Code}) ->
        {"sHml: max fixpoint with necessity ending with recursive 'X'", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'recX(nec.X)',
          ExpCode = "
            formula:mon_max('X',
              formula:mon_nec(
              fun(Action) ->
                case Action of
                  {recv, A, a} ->
                    formula:mon_var('X');
                  _ -> formula:mon_id()
                end
              end
            ))",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"max('X', [A?a]tt)",
      fun(_, {Ccs, Code}) ->
        {"sHml: max fixpoint with necessity ending with tt", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'yes',
          ExpCode = "formula:mon_tt()",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"max('X', [A?a]ff && [B?b]'X')",
      fun(_, {Ccs, Code}) ->
        {"sHml: max fixpoint with conjuction of necessities phi and psi", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'recX(nec.no|nec.X)',
          ExpCode = "
          formula:mon_max('X',
            formula:mon_and(
              formula:mon_nec(
                fun(Action) ->
                  case Action of
                    {recv, A, a} ->
                      formula:mon_ff();
                    _ -> formula:mon_id()
                  end
                end),
              formula:mon_nec(
                fun(Action) ->
                  case Action of
                    {recv, B, b} ->
                      formula:mon_var('X');
                    _ -> formula:mon_id()
                  end
                end
              )))",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"max('X', [A?a][B!b]tt && [B?b] 'X')",
      fun(_, {Ccs, Code}) ->
        {"sHml: max fixpoint with conjunction of necessities phi true and psi", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'recX(nec.X)',
          ExpCode = "
          formula:mon_max('X',
            formula:mon_nec(
              fun(Action) ->
                case Action of
                  {recv, B, b} ->
                    formula:mon_var('X');
                  _ -> formula:mon_id()
                end
              end
            ))",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"max('X', 'X')",
      fun(_, {Ccs, Code}) ->
        {"sHml: max fixpoint on itself", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'recX(X)',
          ExpCode = "formula:mon_max('X', formula:mon_var('X'))",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"/A?a\\tt",
      fun(_, {Ccs, Code}) ->
        {"cHml: possibility ending with tt", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'pos.yes',
          ExpCode = "
            formula:mon_pos(
              fun(Action) ->
                case Action of
                  {recv, A, a} ->
                    formula:mon_tt();
                  _ -> formula:mon_id()
                end
              end
            )",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"/A?a\\/B!b\\tt",
      fun(_, {Ccs, Code}) ->
        {"cHml: multiple possibilities ending with tt", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'pos.pos.yes',
          ExpCode = "
            formula:mon_pos(
              fun(Action) ->
                case Action of
                  {recv, A, a} ->
                    formula:mon_pos(
                      fun(Action) ->
                        case Action of
                          {send, B, b} ->
                            formula:mon_tt();
                          _ -> formula:mon_id()
                        end
                      end
                    );
                  _ -> formula:mon_id()
                end
              end
            )",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"/A?a\\/B!b\\ff",
      fun(_, {Ccs, Code}) ->
        {"cHml: multiple possibilities ending with ff", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'no',
          ExpCode = "formula:mon_ff()",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"/A?a\\tt || /B?b\\ff",
      fun(_, {Ccs, Code}) ->
        {"cHml: disjunction of possibilities with psi equating to false", ?_test(begin

         % The expected CCS and monitor code.
          ExpCcs = 'pos.yes',
          ExpCode = "
            formula:mon_pos(
              fun(Action) ->
                case Action of
                  {recv, A, a} ->
                    formula:mon_tt();
                  _ -> formula:mon_id()
                end
              end
            )",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"/A?a\\ff || /B?b\\tt",
      fun(_, {Ccs, Code}) ->
        {"cHml: disjunction of possibilities with phi equating to false", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'pos.yes',
          ExpCode = "
            formula:mon_pos(
              fun(Action) ->
                case Action of
                  {recv, B, b} ->
                    formula:mon_tt();
                  _ -> formula:mon_id()
                end
              end
            )",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"/A?a\\tt || /B?b\\tt",
      fun(_, {Ccs, Code}) ->
        {"cHml: disjunction of possibilities phi and psi", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'pos.yes|pos.yes',
          ExpCode = "
            formula:mon_or(
              formula:mon_pos(
                fun(Action) ->
                  case Action of
                    {recv, A, a} ->
                      formula:mon_tt();
                    _ -> formula:mon_id()
                  end
                end
              ),
              formula:mon_pos(
                fun(Action) ->
                  case Action of
                    {recv, B, b} ->
                      formula:mon_tt();
                    _ -> formula:mon_id()
                  end
                end
              ))",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"min('X', /A?a\\tt)",
      fun(_, {Ccs, Code}) ->
        {"cHml: min fixpoint with possibility ending with tt", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'recX(pos.yes)',
          ExpCode = "
            formula:mon_min('X',
              formula:mon_pos(
              fun(Action) ->
                case Action of
                  {recv, A, a} ->
                    formula:mon_tt();
                  _ -> formula:mon_id()
                end
              end
            ))",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"min('X', /A?a\\'X')",
      fun(_, {Ccs, Code}) ->
        {"cHml: min fixpoint with possibility ending with recursive 'X'", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'recX(pos.X)',
          ExpCode = "
            formula:mon_min('X',
              formula:mon_pos(
              fun(Action) ->
                case Action of
                  {recv, A, a} ->
                    formula:mon_var('X');
                  _ -> formula:mon_id()
                end
              end
            ))",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"min('X', /A?a\\ff)",
      fun(_, {Ccs, Code}) ->
        {"cHml: min fixpoint with possibility ending with ff", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'no',
          ExpCode = "formula:mon_ff()",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"min('X', /A?a\\tt || /B?b\\'X')",
      fun(_, {Ccs, Code}) ->
        {"cHml: min fixpoint with disjunction of possibilities phi and psi", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'recX(pos.yes|pos.X)',
          ExpCode = "
          formula:mon_min('X',
            formula:mon_or(
              formula:mon_pos(
                fun(Action) ->
                  case Action of
                    {recv, A, a} ->
                      formula:mon_tt();
                    _ -> formula:mon_id()
                  end
                end),
              formula:mon_pos(
                fun(Action) ->
                  case Action of
                    {recv, B, b} ->
                      formula:mon_var('X');
                    _ -> formula:mon_id()
                  end
                end
              )))",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"min('X', /A?a\\/B!b\\ff || /B?b\\ 'X')",
      fun(_, {Ccs, Code}) ->
        {"cHml: min fixpoint with disjunction of possibilities phi false and psi", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'recX(pos.X)',
          ExpCode = "
          formula:mon_min('X',
            formula:mon_pos(
              fun(Action) ->
                case Action of
                  {recv, B, b} ->
                    formula:mon_var('X');
                  _ -> formula:mon_id()
                end
              end
            ))",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
    ,
    {"min('X', 'X')",
      fun(_, {Ccs, Code}) ->
        {"cHml: min fixpoint on itself", ?_test(begin

          % The expected CCS and monitor code.
          ExpCcs = 'recX(X)',
          ExpCode = "formula:mon_min('X', formula:mon_var('X'))",

          % Assert that the generated Ccs representing the monitor, as well case
          % its code is as expected.
          ?assertEqual(ExpCcs, Ccs),
          ?assertEqual(expr_as_str(ExpCode), expr_as_str(Code))
        end)}
      end}
  ]
}.

expr_as_str(Expr) ->
  {ok, Scanned, _} = erl_scan:string(Expr ++ "."),
  {ok, Parsed} = erl_parse:parse_exprs(Scanned),
  lists:flatten(erl_pp:exprs(Parsed)).