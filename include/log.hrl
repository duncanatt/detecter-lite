-ifndef(debug).
-define(debug, true).
-endif.

%%-ifndef(log).
%% When set, enables logging.
-define(log, log).
%%-endif.

-ifndef(log_level).
-define(log_level, 3).
-endif.

-define(trace_level, 1).
-define(debug_level, 2).
-define(info_level, 3).

-define(trace_str, "TRACE").
-define(debug_str, "DEBUG").
-define(info_str, "INFO").

-ifdef(log).
-define(TRACE(Format, Params), log:write(?trace_str, ?MODULE, ?LINE, Format, Params)).
-define(DEBUG(Format, Params), log:write(?debug_str, ?MODULE, ?LINE, Format, Params)).
-define(INFO(Format, Params), log:write(?info_str, ?MODULE, ?LINE, Format, Params)).
-define(TRACE(Format), log:write(?trace_str, ?MODULE, ?LINE, Format)).
-define(DEBUG(Format), log:write(?debug_str, ?MODULE, ?LINE, Format)).
-define(INFO(Format), log:write(?info_str, ?MODULE, ?LINE, Format)).
-else.
-define(TRACE(Format, Params), ok).
-define(DEBUG(Format, Params), ok).
-define(INFO(Format, Params), ok).
-define(TRACE(Format), ok).
-define(DEBUG(Format), ok).
-define(INFO(Format), ok).
-endif.